package com.ramat.origin.detailsmarvelmodule.details

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ramat.origin.detailsmarvelmodule.R
import com.ramat.origin.detailsmarvelmodule.model.Details


class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details_activity)
        if (savedInstanceState == null) {
            val details = Details(name = intent.getStringExtra("name"),
                description = intent.getStringExtra("description"),
                comics = intent.getStringExtra("comics"),
                series = intent.getStringExtra("series"),
                events = intent.getStringExtra("events"),
                stories = intent.getStringExtra("stories"),
                image = intent.getStringExtra("image"),
                linkComics = intent.getStringExtra("comiclink"),
                linkDetails = intent.getStringExtra("detail"),
                linkWiki = intent.getStringExtra("wiki"))

            supportFragmentManager.beginTransaction()
                .replace(R.id.container, DetailsFragment.newInstance(details))
                .commitNow()
        }
    }

}
