package com.ramat.origin.detailsmarvelmodule.model

data class Details(val name: String = "",
                   val description: String = "",
                   val comics: String = "",
                   val series: String = "",
                   val events: String = "",
                   val stories: String = "",
                   val image: String = "",
                   val linkDetails: String = "",
                   val linkWiki: String = "",
                   val linkComics: String = "")