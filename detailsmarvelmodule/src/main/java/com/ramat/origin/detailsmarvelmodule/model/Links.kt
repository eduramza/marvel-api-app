package com.ramat.origin.detailsmarvelmodule.model

data class Links(val type: String, val links: String)