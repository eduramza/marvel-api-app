package com.ramat.origin.marvelheroesapp.ui.home

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ramat.original.moviescatalog.model.ReturnData
import com.ramat.original.moviescatalog.repository.MarvelRepository
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.io.File


class HomeViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    var type = object : TypeToken<ReturnData>() {

    }.type

    val MORE_THAN_100 = "responses/mock_response_error_more_than_100.json"
    val RESPONSE_OK = "responses/mock_response_successfully.json"

    private lateinit var mockHomeViewModel: HomeViewModel
    @Mock
    lateinit var mockRepository: MarvelRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mockHomeViewModel = HomeViewModel(mockRepository)
    }

    @Test
    fun testListHeroesIsNotNull(){
        val path = RESPONSE_OK

        val data = getJson(path)

        var model = Gson().fromJson<ReturnData>(data, type)

        assertNotNull(model.data.results)
    }

    @Test
    fun testMoreThan100Error(){
        val path = MORE_THAN_100
        val data = getJson(path)

        var model = Gson().fromJson<ReturnData>(data, type)

        assertEquals(409, model.code)
    }

    /**
     * Helper function which will load JSON from
     * the path specified
     *
     * @param path : Path of JSON file
     * @return json : JSON from file at given path
     */
    fun getJson(path : String) : String {
        // Load the JSON response
        val uri = this.javaClass.classLoader.getResource(path)
        val file = File(uri.path)
        return String(file.readBytes())
    }


}